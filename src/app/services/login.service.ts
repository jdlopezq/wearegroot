import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private _http: HttpClient) { }
  url = 'https://grood-api-sgrbtbi36a-uc.a.run.app/grood/api/v1/'

  register(datos) {
    return this._http.post(this.url + 'registrar', datos).pipe(map(data => {
      console.log(data);
    }))
  }

  login(datos) {
   return this._http.post(this.url + 'login', datos).pipe(map(data => {
      console.log(data);
    }))
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSelect, AlertController, LoadingController } from '@ionic/angular';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {
data
password=[]
ingredientList
list=[]
ingredients=false
@ViewChild('select', { static: true, read:IonSelect }) select: IonSelect;
  constructor(private router: Router, private loginService:LoginService, 
    public loadingController: LoadingController,
    private alertcontroller: AlertController) {
    this.data=this.router.getCurrentNavigation().extras.state.data
    this.password.push(this.data.password.id)
    console.log(this.data);
    
   }
  imagesIng = [
    { id: '1', name: 'Carne', url: '../../../assets/recursos-fast-food/carne.png' },
    { id: '2', name: 'Lechuga', url: '../../../assets/recursos-fast-food/lechuga.png' },
    { id: '3', name: 'Peperoni', url: '../../../assets/recursos-fast-food/peperoni.png' },
    { id: '4', name: 'Pollo', url: '../../../assets/recursos-fast-food/pollo.png' },
    { id: '5', name: 'Queso', url: '../../../assets/recursos-fast-food/queso.png' },
    { id: '6', name: 'Cebolla', url: '../../../assets/recursos-fast-food/cebolla.png' },
    { id: '7', name: 'Champiñon', url: '../../../assets/recursos-fast-food/champi.png' },
    { id: '8', name: 'Tomate', url: '../../../assets/recursos-fast-food/tomate.png' },
  ]
  ngOnInit() {
  }
  setPassword(event){
    if (event.detail.value) {
     this.list.push(this.imagesIng[event.detail.value-1].name)
    this.ingredientList=this.list.join(', ')
    this.password.push(event.detail.value)
    this.select.value=null  
    }
    if (this.list.length>=6) {
      this.ingredients=true
    }
  }
  resolved(captchaResponse: string) {
    this.register(captchaResponse)
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }
  cancel(){
    this.password=[this.data.password.id]
    this.ingredientList=[]
    console.log(this.password);
    
  }
  register(captcha){
let dataRegister={
  nombre:this.data.nombre,
  apellido: this.data.apellido,
  email: this.data.email,
  password: this.password.join(''),
  captcha:captcha
}
this.presentLoading() 
this.loginService.register(dataRegister).subscribe(data=>{
console.log(data);
this.loadingController.dismiss
this.openTooltip('¡Genial!','Tu usuario ha sido creado con exito')
},
(error)=>{
  this.openTooltip('¡Ups!','Tu usuario no ha sido creado T_T')
})
  }

  async openTooltip(header, message) {
    let alert = await this.alertcontroller.create(
      {
         header: header,
        message: message,
        buttons: [{text: 'Okay!',
        handler: () => {
          this.router.navigate(['home']);
        }}]
      }
    );
     await alert.present();
    //  setTimeout(()=>alert.dismiss(),15000);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();
  }
}

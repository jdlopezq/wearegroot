import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
images=[
  '../../../assets/recursos-fast-food/burrito-completo.png',
  '../../../assets/recursos-fast-food/burguer-completa.png',
  '../../../assets/recursos-fast-food/pizza-completa.png'
]
@ViewChild('Slides', {static:true, read: IonSlides }) slides: IonSlides;
sliderOptions={
  initialSlide: 0,
  speed: 500,
  autoplay:{delay: 1000},
  loop:true,
}

  constructor() { }

  ngOnInit() {
  }

}

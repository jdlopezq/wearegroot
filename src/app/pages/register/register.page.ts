import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup
  
  constructor(private fb: FormBuilder, private router: Router, private navControler: NavController,) { }
  images = [
    { id: '1', url: '../../../assets/recursos-fast-food/burrito-chiquito.png', name:'Burrito' },
    { id: '2', url: '../../../assets/recursos-fast-food/pizza-chiquita.png', name:'Pizza'},
    { id: '3', url: '../../../assets/recursos-fast-food/hamburguesa-chiquita.png', name:'Hamburguesa' }
  ]
  ngOnInit() {
    this.registerForm = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    })
  }
  goPassword() {
    let navigationExtras: NavigationExtras = {
      state: {
        data: this.registerForm.value
      }
    }
    this.router.navigate(['password'], navigationExtras)
  }
  goBack(){
    this.navControler.back();
  }
}

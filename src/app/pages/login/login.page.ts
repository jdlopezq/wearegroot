import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private loginService: LoginService,
    public loadingController: LoadingController,
    private alertcontroller: AlertController) { }
  images = [
    { id: '1', url: '../../../assets/recursos-fast-food/burrito-chiquito.png', name:'Burrito' },
    { id: '2', url: '../../../assets/recursos-fast-food/pizza-chiquita.png', name:'Pizza'},
    { id: '3', url: '../../../assets/recursos-fast-food/hamburguesa-chiquita.png', name:'Hamburguesa' }
  ]
  imagesIng = [
    { id: '1', name: 'Carne', url: '../../../assets/recursos-fast-food/carne.png' },
    { id: '2', name: 'Lechuga', url: '../../../assets/recursos-fast-food/lechuga.png' },
    { id: '3', name: 'Peperoni', url: '../../../assets/recursos-fast-food/peperoni.png' },
    { id: '4', name: 'Pollo', url: '../../../assets/recursos-fast-food/pollo.png' },
    { id: '5', name: 'Queso', url: '../../../assets/recursos-fast-food/queso.png' },
    { id: '6', name: 'Cebolla', url: '../../../assets/recursos-fast-food/cebolla.png' },
    { id: '7', name: 'Champiñon', url: '../../../assets/recursos-fast-food/champi.png' },
    { id: '8', name: 'Tomate', url: '../../../assets/recursos-fast-food/tomate.png' },
  ]
  password = { food: '', ingredients: [] };
  passImage: string
  email: string;
  passLenght
  ngOnInit() {
  }
  selectImage(image) {
    this.passImage = image.url
    this.password['food'] = image.id
  }

  selectIng(image) {
    this.imagesIng[image].id
    this.password['ingredients'].push(this.imagesIng[image].id)
    if (this.password.ingredients.length<=6) {
      this.passLenght=true
    }
    console.log(this.password);
  }

  clearPass() {
    this.password['ingredients'] = []
  }
  resolved(captchaResponse: string) {
    this.logIn(captchaResponse)
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  logIn(captcha) {
    let data= this.password.ingredients.join('')
    let sendInfo = {
      email: this.email,
      password: this.password.food+data,
      captcha: captcha
    }
    this.presentLoading()
    this.loginService.login(sendInfo).subscribe(data => {
      console.log(data);
      this.loadingController.dismiss()
      this.openTooltip('Exito', 'Login Exitoso')
    },
    (error)=>{
      this.openTooltip('Asi no te gusta...', 'Sabemos que asi no es tu comida favorita :(')
    })

  }

  async openTooltip(header, message) {
    let alert = await this.alertcontroller.create(
      {
         header: header,
        message: message,
        buttons: ['Ok']
      }
    );
     await alert.present();
    //  setTimeout(()=>alert.dismiss(),15000);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();
  }
}
